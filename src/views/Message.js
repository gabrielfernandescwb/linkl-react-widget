import React from "react";
import root from "react-shadow";
import styles from "!raw-loader!../styled.css";

import http from "http";
import { linklmsg } from "../utils/Apis";
import { myVariables } from "../utils/GlobalVariables";

import InputMask from "react-input-mask";

import SuccessTab from "./Success";

export default class MessageTab extends React.Component {
  constructor() {
    super();
    this.state = {
      userName: "",
      userPhone: "",
      userEmail: "",
      userMessage: "",
      success: false
    };
    this.userMessage = this.userMessage.bind(this);
  }

  getName(e) {
    this.setState({
      userName: e.target.value
    });
  }

  getPhone(e) {
    this.setState({
      userPhone: e.target.value
    });
  }

  getEmail(e) {
    this.setState({
      userEmail: e.target.value
    });
  }

  getMessage(e) {
    this.setState({
      userMessage: e.target.value
    });
  }

  userMessage() {
    const { userName, userPhone, userEmail, userMessage } = this.state;

    var req = http.request(
      linklmsg,
      function(res) {
        var chunks = [];

        res.on("data", function(chunk) {
          chunks.push(chunk);
        });

        res.on(
          "end",
          function() {
            this.setState({
              success: true
            });
            setTimeout(
              function() {
                this.setState({
                  success: false
                });
              }.bind(this),
              2000
            );
          }.bind(this)
        );
      }.bind(this)
    );

    req.write(
      '{"codigo":"' +
        myVariables[1] +
        '","nome":"' +
        userName +
        '","fone":"' +
        userPhone +
        '","email":"' +
        userEmail +
        '","mensagem":"' +
        userMessage +
        '"}'
    );
    req.end();
  }

  render() {
    const { success } = this.state;
    return (
      <root.div className="container-inner">
        {success === false ? (
          <div className="tab-msg">
            <p className="tab-title">
              Deixe sua mensagem e retornaremos em breve.
            </p>
            <input
              className="tab-input__name-msg"
              type="text"
              placeholder="Nome"
              onInput={e => this.getName(e)}
            />
            <InputMask
              className="tab-input__phone-msg"
              mask="(99) 99999 - 9999"
              maskChar=" "
              placeholder="Telefone"
              onInput={e => this.getPhone(e)}
            />
            <input
              className="tab-input__email-msg"
              maskChar=" "
              placeholder="Email"
              onInput={e => this.getEmail(e)}
            />
            <textarea
              className="tab-input__text-msg"
              placeholder="Mensagem ..."
              onInput={e => this.getMessage(e)}
            />
            <div className="tab-button">
              <button className="tab-button__button" onClick={this.userMessage}>
                Enviar mensagem
              </button>
            </div>
            <style type="text/css">{styles}</style>
          </div>
        ) : (
          <div className="tab-msg">
            <SuccessTab />
          </div>
        )}
      </root.div>
    );
  }
}
